---
title: "Astrophotography"
---

<br>

## The Moon

![Taken January 2, 2012 at 5:57PM](content/astrophotography/moon-zoom.jpeg)

![Taken November 12, 2011 at 7:06PM](content/astrophotography/moon.jpeg)

## Planets

### Saturn

![Taken March 9, 2021 at 11:13PM](content/astrophotography/saturn.jpeg)
