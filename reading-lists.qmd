---
title: "Reading Lists"
---

```{r setup, include=FALSE}
library(knitr)
library(readr)
library(dplyr)
library(stringr)
```

```{r import-data, include=FALSE}
books <- read_csv('data/reading.csv')
```

<br>

# Currently Reading

```{r currently-reading, echo=FALSE}
currently_reading <- books %>%
    filter(status == 'reading') %>%
    select(title, author, type)

kable(currently_reading,col.names = str_to_title(colnames(currently_reading)))
```

<br>

# Finshed Reading

<br>

## 2021

<br>

### Non-fiction

```{r 2021-non-fiction, echo=FALSE}
read_2021_nonfiction <- books %>%
    filter(status == 'read', year == 2021, type=="Non-fiction") %>%
    select(title, author, rating)

kable(read_2021_nonfiction,col.names = str_to_title(colnames(read_2021_nonfiction)))
```

### Fiction

```{r 2021-fiction, echo=FALSE}
read_2021_fiction <- books %>%
    filter(status == 'read', year == 2021, type=="Fiction") %>%
    select(title, author, rating)

kable(read_2021_fiction,col.names = str_to_title(colnames(read_2021_fiction)))
```

<br>

## 2020

<br>

### Non-fiction

```{r 2020-non-fiction, echo=FALSE}
read_2020_nonfiction <- books %>%
    filter(status == 'read', year == 2020, type=="Non-fiction") %>%
    select(title, author, rating)

kable(read_2020_nonfiction,col.names = str_to_title(colnames(read_2020_nonfiction)))
```

### Fiction

```{r 2020-fiction, echo=FALSE}
read_2020_fiction <- books %>%
    filter(status == 'read', year == 2020, type=="Fiction") %>%
    select(title, author, rating)

kable(read_2020_fiction,col.names = str_to_title(colnames(read_2020_fiction)))
```

<br>

## 2019

<br>

### Non-fiction

```{r 2019-non-fiction, echo=FALSE}
read_2019_nonfiction <- books %>%
    filter(status == 'read', year == 2019, type=="Non-fiction") %>%
    select(title, author, rating)

kable(read_2019_nonfiction,col.names = str_to_title(colnames(read_2019_nonfiction)))
```

<br>

## Past Years

<br>

### Non-fiction

```{r past-non-fiction, echo=FALSE}
read_past_nonfiction <- books %>%
    filter(status == 'read', year < 2019, type=="Non-fiction") %>%
    select(title, author, rating)

kable(read_past_nonfiction,col.names = str_to_title(colnames(read_past_nonfiction)))
```

### Fiction

```{r past-fiction, echo=FALSE}
read_past_fiction <- books %>%
    filter(status == 'read', year < 2019, type=="Fiction") %>%
    select(title, author, rating)

kable(read_past_fiction,col.names = str_to_title(colnames(read_past_fiction)))
```
